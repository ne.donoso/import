const modulo1 ={};

function sumar(x1, x2){
    return x1 + x2;
}

function multiplicar(x1, x2){
    return x1 * x2;
}

function dividir(x1, x2){
    if(x2===0){
        console.log('no se puede dividir por cero')
    } else{
        return x1 / x2;
    }
    
}


modulo1.sumar = sumar;
modulo1.multiplicar = multiplicar;
modulo1.dividir = dividir;

module.exports = modulo1;

/*
exports.sumar = sumar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;
*/
